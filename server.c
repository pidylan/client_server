/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <time.h>
#include <ctype.h>
// method to throw an error.
void error(const char *msg)
{
    perror(msg);
    exit(1);
}

// function that writes to file
void Write_To_File(char buffer[]) {

	// array van char aanmaken voor de file naam.
	char file_name[50];

	// variabele t reserveren voor de tijd.
	time_t t = time(NULL);

	// propertie tm van struc tm invullen met de locale tijd.
	struct tm *tm = localtime(&t);
	int i = 0;

	// De file name opslaan met als extensie .txt en als naam de huidige tijd.
	sprintf(file_name, "%s.txt", asctime(tm));

	// replace space with underscore.
	while (file_name[i])
	{
		if (isspace(file_name[i]))
			file_name[i] = '_';
		i++;
	}

	// Naar de file schrijven (w = write).
	FILE *f = fopen(file_name, "w");

	// indien de file niet kan geopend worden, schrijf dan een error.
	if (f == NULL)
	{
		printf("Error openening file!\n");
		exit(1);
	}

	// de data van de buffer naar de file schrijven.
	fprintf(f, "%s\n", buffer);

	// FILE correct afsluiten;
	fclose(f);
}




// Creating Child process
void Child_Process(int newsockfd)
{
	
    int n;
	int len = 0;



	// creating new child process and gets the buffer size from the client.
	ioctl(newsockfd, FIONREAD, &len);

	char buffer[len];

	// Bzero() places n zero-valued bytes in the buffer.
    bzero(buffer, len);


	if (len > 0) {
		len = read(newsockfd, buffer, len);
	}


	// n return the number of bytes that were read. If value is negative, then the system call returned an error.
	// The data form the read while be stored in the buffer.
	// 255 is the number of bytes to read before truncating the data. if the data to be read is smaller than nbytes, all data is saved in the buffer.
    n = read(newsockfd, buffer, sizeof(buffer));

	// schrijf de buffer naar de file.
	Write_To_File(buffer);

	// If value if n = negative, then the system call returned an error.
    if (n<0) error("ERROR reading from socket");


}

int main(int argc, char *argv[])
{
	// sockets file descriptors and port nr. declaration variables.
     int sockfd, newsockfd, portno;

	 // variable for size of adress client.
     socklen_t clilen;

	 // datatype that represents the process id's.
     pid_t pid;


     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;

	 // when arguments are less than 2 give a error. 
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));

     portno = atoi(argv[1]);

     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
     serv_addr.sin_port = htons(portno);

     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");

     listen(sockfd,5);
     clilen = sizeof(cli_addr);

	 // use while-lus so that the server keeps listening.
  while (1) {

    newsockfd = accept(sockfd,
      (struct sockaddr * ) & cli_addr, &
      clilen);

    if (newsockfd < 0)
      error("ERROR on accept");

    else {

		// use method fork() to create multiple processes.
		// when process ID = 0 then this is a child process.
      pid = fork();
      if (pid == 0) {
        printf("received file\r\n");
        close(sockfd);
        Child_Process(newsockfd);
       
        exit(0);

		// when process id is greater than 0 
		// then process = parent process.
      } else if (pid > 0) {

        close(newsockfd);
      
      } else if (pid < 0) {
        printf("error");
      }

    }

    if (n < 0) error("ERROR writing to socket");
  }
     close(newsockfd);
     close(sockfd);
     return 0; 
}
