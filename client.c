#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

// function to read the file that needs to be buffered.
void File_To_buffer(char * argv, char** buffer_file) {
	FILE *infile;
	long numbytes;

	infile = fopen(argv, "r");

	if (infile == NULL) error("Error file");

	fseek(infile, 0L, SEEK_END);
	numbytes = ftell(infile);
	fseek(infile, 0L, SEEK_SET);
	*buffer_file = (char*)calloc(numbytes, sizeof(char));
	if (*buffer_file == NULL) error("ERROR reading from file");

	fread(*buffer_file, sizeof(char), numbytes, infile);
	fclose(infile);
}




int main(int argc, char *argv[])
{

	// buffer with data from file
	char *buffer_file;

	// declaring some variables
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

	// declaring the buffer we're going to use to store the data to send.
    char buffer[256];
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port file\n", argv[0]);
       exit(0);
    }


	// file to buffer
	File_To_buffer(argv[3], &buffer_file);




	// connect to socket server.
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");

    printf("Sending file:%s \r\n", argv[3]);
    // send buffer naar de server
    n = write(sockfd, buffer_file, strlen(buffer_file));

    printf("Sending: %s", buffer_file);
    free(buffer_file);

    if (n < 0) 
         error("ERROR writing to socket");

	
    close(sockfd);
    return 0;
}
